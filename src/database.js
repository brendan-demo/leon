const db = firebase.firestore();

function getDocId() {
    const url = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    return url ? url : 'callanan';
}

function writeLeon(leon) {
    db.collection("games").doc(getDocId()).set({
        leon,
    })
    .then(function() {
        console.log("Document successfully written!");
    })
    .catch(function(error) {
        console.error("Error writing document: ", error);
    });
}

function dataUpdate(cb,) {
    db.collection("games").doc(getDocId())
    .onSnapshot(function(doc) {
        var source = doc.metadata.hasPendingWrites ? "Local" : "Server";
        //console.log(source, " data: ", doc.data());
        if (source == 'Server') cb(doc.data());
    });
}

function getNewDoc() {
    return db.collection("games").doc().id;
}

export {writeLeon, dataUpdate, getNewDoc}